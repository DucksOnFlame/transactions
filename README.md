# Transactions app

### Running the app
In order to run the app first make sure you are using Java 14. Also make sure that your 8080 and 27017 ports are not currently in use.

Then use the prepared script to run the app. Navigate to the project's main catalog and run:

Mac Os/Linux:

``` $ sh run-app.sh ```

Windows:

``` $ bash run-app.sh ```

If you only want to execute tests run the following command in the project folder:

``` $ gradle test ```

When the app is up and running, in order to retrieve transactions use the following endpoint:

``` GET https://localhost:8080/transactions?account_type=1&customer_id=1 ```

Query params ```account_type``` and ```customer_id``` are optional and also can take the value of ```ALL``` or a comma-separated list of ids.
For authentication use BASIC auth and use credentials from auth.properties (key is username, value is password). No encryption is used in this case.