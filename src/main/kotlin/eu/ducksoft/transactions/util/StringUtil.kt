package eu.ducksoft.transactions.util

fun String.asIdsOrThrow(): List<Int> {
    if (isBlank()) {
        return emptyList()
    }

    return this.split(",")
            .map { it.toInt() }
            .toList()
}