package eu.ducksoft.transactions.service

import java.math.BigDecimal

data class Transaction(val date: String,
                       val amount: BigDecimal,
                       val id: Int,
                       val accountType: String,
                       val customerFirstName: String,
                       val customerLastName: String) : Comparable<Transaction> {

    override fun compareTo(other: Transaction): Int {
        return amount.compareTo(other.amount)
    }
}
