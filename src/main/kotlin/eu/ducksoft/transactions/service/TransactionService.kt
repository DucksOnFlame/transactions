package eu.ducksoft.transactions.service

import eu.ducksoft.transactions.infrastructure.AccountTypeRepository
import eu.ducksoft.transactions.infrastructure.CustomerRepository
import eu.ducksoft.transactions.infrastructure.TransactionRepository
import eu.ducksoft.transactions.infrastructure.entity.AccountTypeEntity
import eu.ducksoft.transactions.infrastructure.entity.CustomerEntity
import eu.ducksoft.transactions.infrastructure.entity.TransactionEntity
import org.springframework.stereotype.Service

@Service
class TransactionService(private val accountTypeRepository: AccountTypeRepository,
                         private val customerRepository: CustomerRepository,
                         private val transactionRepository: TransactionRepository) {

    fun getTransactions(accountTypes: List<Int>, customerIds: List<Int>): List<Transaction> {
        val transactionEntities: Iterable<TransactionEntity> = findTransactions(accountTypes, customerIds)
        val foundAccountTypeIds: List<Int> = transactionEntities.map { it.accountTypeId }.distinct()
        val foundCustomerIds: List<Int> = transactionEntities.map { it.customerId }.distinct()

        val accountEntitiesMap: Map<Int, AccountTypeEntity> = accountTypeRepository.findAllById(foundAccountTypeIds)
                .map { it.id to it }
                .toMap()
        val customerEntitiesMap: Map<Int, CustomerEntity> = customerRepository.findAllById(foundCustomerIds)
                .map { it.id to it }
                .toMap()

        return transactionEntities.map { toTransaction(it, accountEntitiesMap[it.accountTypeId], customerEntitiesMap[it.customerId]) }
                .sorted()
    }

    private fun findTransactions(accountTypes: List<Int>, customerIds: List<Int>): Iterable<TransactionEntity> {
        return when {
            accountTypes.isNotEmpty() && customerIds.isNotEmpty() -> transactionRepository.findByCustomerIdInAndAccountTypeIdIn(customerIds, accountTypes)
            accountTypes.isNotEmpty() -> transactionRepository.findByAccountTypeIdIn(accountTypes)
            customerIds.isNotEmpty() -> transactionRepository.findByCustomerIdIn(customerIds)
            else -> transactionRepository.findAll()
        }
    }

    private fun toTransaction(transaction: TransactionEntity, accountType: AccountTypeEntity?, customer: CustomerEntity?): Transaction {
        return Transaction(
                transaction.transactionDate,
                transaction.amount,
                transaction.id,
                accountType?.name ?: "",
                customer?.firstName ?: "",
                customer?.lastName ?: ""
        )
    }
}
