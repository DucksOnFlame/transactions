package eu.ducksoft.transactions.infrastructure

import eu.ducksoft.transactions.infrastructure.entity.TransactionEntity
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.data.repository.CrudRepository

interface TransactionRepository : MongoRepository<TransactionEntity, Int> {

    fun findByCustomerIdIn(customerIds: List<Int>): Iterable<TransactionEntity>

    fun findByAccountTypeIdIn(accountTypeIds: List<Int>): Iterable<TransactionEntity>

    fun findByCustomerIdInAndAccountTypeIdIn(customerIds: List<Int>, accountTypeIds: List<Int>): Iterable<TransactionEntity>
}