package eu.ducksoft.transactions.infrastructure

import eu.ducksoft.transactions.infrastructure.entity.CustomerEntity
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.data.repository.CrudRepository

interface CustomerRepository : MongoRepository<CustomerEntity, Int> {
}