package eu.ducksoft.transactions.infrastructure.entity

import eu.ducksoft.transactions.infrastructure.ACCOUNT_TYPE_COLLECTION_NAME
import org.springframework.data.mongodb.core.mapping.Document
import javax.persistence.Id

@Document(collection = ACCOUNT_TYPE_COLLECTION_NAME)
data class AccountTypeEntity(@Id val id: Int,
                             val name: String)