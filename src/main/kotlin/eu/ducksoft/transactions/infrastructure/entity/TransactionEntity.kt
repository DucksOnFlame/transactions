package eu.ducksoft.transactions.infrastructure.entity

import eu.ducksoft.transactions.infrastructure.TRANSACTION_COLLECTION_NAME
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document
import java.math.BigDecimal
import javax.persistence.Id

@Document(collection = TRANSACTION_COLLECTION_NAME)
data class TransactionEntity(@Id val id: Int,
                             val amount: BigDecimal,
                             @Indexed val accountTypeId: Int,
                             @Indexed val customerId: Int,
                             val transactionDate: String)