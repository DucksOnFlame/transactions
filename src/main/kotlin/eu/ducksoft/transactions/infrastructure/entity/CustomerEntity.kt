package eu.ducksoft.transactions.infrastructure.entity

import eu.ducksoft.transactions.infrastructure.CUSTOMER_COLLECTION_NAME
import org.springframework.data.mongodb.core.mapping.Document
import java.math.BigDecimal
import javax.persistence.Id

@Document(collection = CUSTOMER_COLLECTION_NAME)
data class CustomerEntity(@Id val id: Int,
                          val firstName: String,
                          val lastName: String,
                          val lastLoginBalance: BigDecimal)