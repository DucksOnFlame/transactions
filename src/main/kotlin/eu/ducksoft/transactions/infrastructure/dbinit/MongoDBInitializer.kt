package eu.ducksoft.transactions.infrastructure.dbinit

import eu.ducksoft.transactions.infrastructure.ACCOUNT_TYPE_COLLECTION_NAME
import eu.ducksoft.transactions.infrastructure.CUSTOMER_COLLECTION_NAME
import eu.ducksoft.transactions.infrastructure.TRANSACTION_COLLECTION_NAME
import eu.ducksoft.transactions.infrastructure.entity.TransactionEntity
import org.springframework.data.domain.Sort
import org.springframework.data.mongodb.core.MongoOperations
import org.springframework.data.mongodb.core.index.Index
import org.springframework.stereotype.Service

@Service
class MongoDBInitializer(private val mongoOps: MongoOperations,
                         dataInitializer: DataInitializer) {

    init {
        recreateCollection(CUSTOMER_COLLECTION_NAME)
        recreateCollection(ACCOUNT_TYPE_COLLECTION_NAME)
        recreateCollection(TRANSACTION_COLLECTION_NAME)
        initializeTransactionIndexes()
        dataInitializer.initializeData()
    }

    private fun recreateCollection(collectionName: String) {
        if (mongoOps.collectionExists(collectionName)) {
            mongoOps.dropCollection(collectionName)
        }

        mongoOps.createCollection(collectionName)
    }

    private fun initializeTransactionIndexes() {
        mongoOps.indexOps(TransactionEntity::class.java)
                .ensureIndex(Index().on("accountTypeId", Sort.Direction.ASC))
        mongoOps.indexOps(TransactionEntity::class.java)
                .ensureIndex(Index().on("customerId", Sort.Direction.ASC))
    }
}