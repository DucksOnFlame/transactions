package eu.ducksoft.transactions.infrastructure.dbinit

import eu.ducksoft.transactions.infrastructure.AccountTypeRepository
import eu.ducksoft.transactions.infrastructure.CustomerRepository
import eu.ducksoft.transactions.infrastructure.TransactionRepository
import eu.ducksoft.transactions.infrastructure.entity.AccountTypeEntity
import eu.ducksoft.transactions.infrastructure.entity.CustomerEntity
import eu.ducksoft.transactions.infrastructure.entity.TransactionEntity
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.io.BufferedReader
import java.io.FileReader
import java.math.BigDecimal
import java.util.*

private const val COMMA_DELIMITER = ","
private const val CUSTOMERS_FILE_PATH = "src/main/resources/data/customers.csv"
private const val ACCOUNT_TYPES_FILE_PATH = "src/main/resources/data/accounttypes.csv"
private const val TRANSACTIONS_FILE_PATH = "src/main/resources/data/transactions.csv"

@Service
class DataInitializer(private val customerRepository: CustomerRepository,
                      private val accountTypeRepository: AccountTypeRepository,
                      private val transactionRepository: TransactionRepository) {

    val logger: Logger = LoggerFactory.getLogger(DataInitializer::class.java)


    fun initializeData() {
        initAccountTypes()
        initCustomers()
        initTransactions()
    }

    private fun initAccountTypes() {
        logger.info("Initializing account types.")
        val csvRecords: List<List<String>> = readRecordsFromFileSkippingHeader(ACCOUNT_TYPES_FILE_PATH)
        for (record in csvRecords) {
            val accountType = AccountTypeEntity(
                    record[ACCOUNT_TYPE_ID].toInt(),
                    record[ACCOUNT_TYPE_NAME]
            )

            accountTypeRepository.save(accountType)
        }
        logger.info("Finished saving account types.")
    }

    private fun initCustomers() {
        logger.info("Initializing customers.")
        val csvRecords: List<List<String>> = readRecordsFromFileSkippingHeader(CUSTOMERS_FILE_PATH)
        for (record in csvRecords) {
            val customer = CustomerEntity(
                    record[CUSTOMER_ID].toInt(),
                    record[CUSTOMER_FIRST_NAME],
                    record[CUSTOMER_LAST_NAME],
                    BigDecimal(record[CUSTOMER_LAST_LOGIN_BALANCE])
            )

            customerRepository.save(customer)
        }
        logger.info("Finished saving customers.")
    }

    private fun initTransactions() {
        logger.info("Initializing transactions.")
        val csvRecords: List<List<String>> = readRecordsFromFileSkippingHeader(TRANSACTIONS_FILE_PATH)
        for (record in csvRecords) {
            val transaction = TransactionEntity(
                    record[TRANSACTION_ID].toInt(),
                    BigDecimal(record[TRANSACTION_AMOUNT]),
                    record[TRANSACTION_TYPE].toInt(),
                    record[TRANSACTION_CUSTOMER_ID].toInt(),
                    record[TRANSACTION_DATE]
            )

            transactionRepository.save(transaction)
        }
        logger.info("Finished saving transactions.")
    }

    private fun readRecordsFromFileSkippingHeader(filePath: String): List<List<String>> {
        val records: MutableList<List<String>> = ArrayList()
        BufferedReader(FileReader(filePath)).use { br ->
            // skip header
            br.readLine()
            var line: String?
            while (br.readLine().also { line = it } != null) {
                val values: Array<String> = line!!.split(COMMA_DELIMITER).toTypedArray()
                records.add(values.asList())
            }
        }

        return records
    }
}