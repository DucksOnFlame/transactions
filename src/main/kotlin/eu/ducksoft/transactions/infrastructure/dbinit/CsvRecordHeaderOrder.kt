package eu.ducksoft.transactions.infrastructure.dbinit

const val CUSTOMER_ID = 0
const val CUSTOMER_FIRST_NAME = 1
const val CUSTOMER_LAST_NAME = 2
const val CUSTOMER_LAST_LOGIN_BALANCE = 3

const val ACCOUNT_TYPE_ID = 0
const val ACCOUNT_TYPE_NAME = 1

const val TRANSACTION_ID = 0
const val TRANSACTION_AMOUNT = 1
const val TRANSACTION_TYPE = 2
const val TRANSACTION_CUSTOMER_ID = 3
const val TRANSACTION_DATE = 4
