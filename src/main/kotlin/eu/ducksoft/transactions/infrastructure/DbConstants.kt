package eu.ducksoft.transactions.infrastructure

const val TRANSACTION_COLLECTION_NAME = "transaction"
const val CUSTOMER_COLLECTION_NAME = "customer"
const val ACCOUNT_TYPE_COLLECTION_NAME = "accountType"
