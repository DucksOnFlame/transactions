package eu.ducksoft.transactions.infrastructure

import eu.ducksoft.transactions.infrastructure.entity.AccountTypeEntity
import org.springframework.data.mongodb.repository.MongoRepository

interface AccountTypeRepository : MongoRepository<AccountTypeEntity, Int> {
}