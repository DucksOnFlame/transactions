package eu.ducksoft.transactions.rest

import eu.ducksoft.transactions.rest.dto.TransactionDTO
import eu.ducksoft.transactions.service.Transaction

fun mapToDTO(transaction: Transaction): TransactionDTO {
    return TransactionDTO(
            transaction.date,
            transaction.amount,
            transaction.id,
            transaction.accountType,
            transaction.customerFirstName,
            transaction.customerLastName
    )
}