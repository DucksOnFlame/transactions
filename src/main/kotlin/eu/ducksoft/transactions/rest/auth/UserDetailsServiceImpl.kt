package eu.ducksoft.transactions.rest.auth

import org.springframework.context.annotation.PropertySource
import org.springframework.core.env.Environment
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service
import java.util.*
import kotlin.collections.HashMap


@Service
@PropertySource("classpath:auth.properties")
class UserDetailsServiceImpl(private val env: Environment) : UserDetailsService {

    private val cache: HashMap<String, Optional<String>> = HashMap()

    fun getPassword(userName: String): Optional<String> {
        if (cache.containsKey(userName)) {
            return cache[userName]!!
        }

        val password: Optional<String> = Optional.ofNullable(env.getProperty(userName))
        cache[userName] = password
        return password
    }

    override fun loadUserByUsername(username: String?): UserDetails {
        if (username == null) {
            throw UsernameNotFoundException("Username is null!")
        }

        val grantedAuthorities: Set<GrantedAuthority> = HashSet()
        val password: String = getPassword(username)
                .orElseThrow { UsernameNotFoundException("Could not find user: $username") }
        return User(username, password, grantedAuthorities)
    }
}