package eu.ducksoft.transactions.rest.dto

import java.math.BigDecimal

data class TransactionDTO(val date: String,
                          val amount: BigDecimal,
                          val id: Int,
                          val accountType: String,
                          val customerFirstName: String,
                          val customerLastName: String)