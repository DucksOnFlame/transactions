package eu.ducksoft.transactions.rest

import eu.ducksoft.transactions.rest.dto.TransactionDTO
import eu.ducksoft.transactions.rest.exception.InvalidIdParamException
import eu.ducksoft.transactions.service.TransactionService
import eu.ducksoft.transactions.util.asIdsOrThrow
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class TransactionController(val transactionService: TransactionService) {

    @GetMapping(value = ["/transactions"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getTransactions(@RequestParam("account_type") accountTypeIdsString: String?,
                        @RequestParam("customer_id") customerIdsString: String?): List<TransactionDTO> {
        val accountTypeIds = asIdsOrThrowBadRequest(accountTypeIdsString)
        val customerIds = asIdsOrThrowBadRequest(customerIdsString)
        return transactionService.getTransactions(accountTypeIds, customerIds)
                .map { mapToDTO(it) }
    }
}

fun asIdsOrThrowBadRequest(idsString: String?): List<Int> {
    if (idsString == "ALL") {
        return emptyList()
    }

    try {
        return idsString?.asIdsOrThrow() ?: emptyList()
    } catch (e: NumberFormatException) {
        throw InvalidIdParamException()
    }
}
