package eu.ducksoft.transactions

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories

@EnableMongoRepositories
@SpringBootApplication
class TransactionsApplication

fun main(args: Array<String>) {
    runApplication<TransactionsApplication>(*args)
}
