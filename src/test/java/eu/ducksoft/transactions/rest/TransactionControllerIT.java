package eu.ducksoft.transactions.rest;

import static eu.ducksoft.transactions.infrastructure.DbConstantsKt.ACCOUNT_TYPE_COLLECTION_NAME;
import static eu.ducksoft.transactions.infrastructure.DbConstantsKt.CUSTOMER_COLLECTION_NAME;
import static eu.ducksoft.transactions.infrastructure.DbConstantsKt.TRANSACTION_COLLECTION_NAME;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import eu.ducksoft.transactions.infrastructure.entity.AccountTypeEntity;
import eu.ducksoft.transactions.infrastructure.entity.CustomerEntity;
import eu.ducksoft.transactions.infrastructure.entity.TransactionEntity;
import eu.ducksoft.transactions.rest.dto.TransactionDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@SpringBootTest
@ExtendWith(SpringExtension.class)
public class TransactionControllerIT {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private TransactionController controller;

    @BeforeEach
    public void setup() {
        mongoTemplate.dropCollection(TRANSACTION_COLLECTION_NAME);
        mongoTemplate.dropCollection(CUSTOMER_COLLECTION_NAME);
        mongoTemplate.dropCollection(ACCOUNT_TYPE_COLLECTION_NAME);
        mongoTemplate.createCollection(TRANSACTION_COLLECTION_NAME);
        mongoTemplate.createCollection(CUSTOMER_COLLECTION_NAME);
        mongoTemplate.createCollection(ACCOUNT_TYPE_COLLECTION_NAME);

        buildDefaultAccountTypeEntities().forEach(mongoTemplate::save);
        buildDefaultCustomerEntities().forEach(mongoTemplate::save);
        buildDefaultTransactionEntities().forEach(mongoTemplate::save);
    }

    @Test
    public void givenQueryParamsAll_whenRequestingAllTransactions_returnsAllTransactions() {
        // given
        String all = "ALL";

        // when
        List<TransactionDTO> transactionDTOs = controller.getTransactions(all, all);

        // then
        assertEquals(buildDefaultTransactionDTOs(), transactionDTOs);
    }

    @Test
    public void givenNoQueryParams_whenRequestingAllTransactions_returnsAllTransactions() {
        // given
        String param = null;

        // when
        List<TransactionDTO> transactionDTOs = controller.getTransactions(param, param);

        // then
        assertEquals(buildDefaultTransactionDTOs(), transactionDTOs);
    }

    @Test
    public void givenAccountTypeQueryParam_returnsTransactionsFilteredByAccountType() {
        // given
        String accountTypeIdsString = "1,2";

        // when
        List<TransactionDTO> transactionDTOs = controller.getTransactions(accountTypeIdsString, null);

        // then
        Set<String> accountTypeNames = new HashSet<>();
        accountTypeNames.add("saving_account");
        accountTypeNames.add("checking_account");
        assertEquals(buildDefaultTransactionDTOs().stream()
                                                  .filter(transaction -> accountTypeNames.contains(transaction.getAccountType()))
                                                  .collect(Collectors.toList()),
                     transactionDTOs);
    }

    @Test
    public void givenCustomerIdQueryParam_returnsTransactionsFilteredByCustomers() {
        // given
        String customerIdsString = "1";

        // when
        List<TransactionDTO> transactionDTOs = controller.getTransactions(null, customerIdsString);

        // then
        assertEquals(buildDefaultTransactionDTOs().stream()
                                                  .filter(transaction -> "Kowalski".equals(transaction.getCustomerLastName()))
                                                  .collect(Collectors.toList()),
                     transactionDTOs);
    }

    @Test
    public void givenBothParams_returnsTransactionsFilteredByCustomersAndAccountTypes() {
        // given
        String customerIdsString = "1";
        String accountTypeIdsString = "1";

        // when
        List<TransactionDTO> transactionDTOs = controller.getTransactions(accountTypeIdsString, customerIdsString);

        // then
        assertEquals(buildDefaultTransactionDTOs().stream()
                                                  .filter(transaction -> "Kowalski".equals(transaction.getCustomerLastName()))
                                                  .filter(transaction -> "saving_account".equals(transaction.getAccountType()))
                                                  .collect(Collectors.toList()),
                     transactionDTOs);
    }

    private List<TransactionEntity> buildDefaultTransactionEntities() {
        List<TransactionEntity> result = new ArrayList<>();
        result.add(new TransactionEntity(
            1,
            new BigDecimal(1000),
            1,
            1,
            "2020-12-05"
        ));

        result.add(new TransactionEntity(
            2,
            new BigDecimal(2000),
            2,
            1,
            "2020-12-04"
        ));

        result.add(new TransactionEntity(
            3,
            new BigDecimal(3000),
            1,
            2,
            "2020-12-06"
        ));

        result.add(new TransactionEntity(
            4,
            new BigDecimal(4000),
            2,
            2,
            "2020-12-10"
        ));

        result.add(new TransactionEntity(
            5,
            new BigDecimal(5000),
            3,
            2,
            "2020-12-11"
        ));

        return result;
    }

    private List<CustomerEntity> buildDefaultCustomerEntities() {
        List<CustomerEntity> result = new ArrayList<>();
        result.add(new CustomerEntity(
            1,
            "Jan",
            "Kowalski",
            new BigDecimal(1000)
        ));

        result.add(new CustomerEntity(
            2,
            "Andrzej",
            "Nowak",
            new BigDecimal(2000)
        ));

        return result;
    }

    private List<AccountTypeEntity> buildDefaultAccountTypeEntities() {
        List<AccountTypeEntity> result = new ArrayList<>();
        result.add(new AccountTypeEntity(
            1,
            "saving_account"
        ));

        result.add(new AccountTypeEntity(
            2,
            "checking_account"
        ));

        result.add(new AccountTypeEntity(
            3,
            "trading_account"
        ));

        return result;
    }

    private List<TransactionDTO> buildDefaultTransactionDTOs() {
        List<TransactionDTO> result = new ArrayList<>();
        result.add(new TransactionDTO(
            "2020-12-05",
            new BigDecimal(1000),
            1,
            "saving_account",
            "Jan",
            "Kowalski"
        ));

        result.add(new TransactionDTO(
            "2020-12-04",
            new BigDecimal(2000),
            2,
            "checking_account",
            "Jan",
            "Kowalski"
        ));

        result.add(new TransactionDTO(
            "2020-12-06",
            new BigDecimal(3000),
            3,
            "saving_account",
            "Andrzej",
            "Nowak"
        ));

        result.add(new TransactionDTO(
            "2020-12-10",
            new BigDecimal(4000),
            4,
            "checking_account",
            "Andrzej",
            "Nowak"
        ));

        result.add(new TransactionDTO(
            "2020-12-11",
            new BigDecimal(5000),
            5,
            "trading_account",
            "Andrzej",
            "Nowak"
        ));

        return result;
    }

}
