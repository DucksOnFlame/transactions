package eu.ducksoft.transactions.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import eu.ducksoft.transactions.infrastructure.AccountTypeRepository;
import eu.ducksoft.transactions.infrastructure.CustomerRepository;
import eu.ducksoft.transactions.infrastructure.TransactionRepository;
import eu.ducksoft.transactions.infrastructure.entity.AccountTypeEntity;
import eu.ducksoft.transactions.infrastructure.entity.CustomerEntity;
import eu.ducksoft.transactions.infrastructure.entity.TransactionEntity;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

public class TransactionServiceTest {

    @Mock
    private TransactionRepository transactionRepository;

    @Mock
    private AccountTypeRepository accountTypeRepository;

    @Mock
    private CustomerRepository customerRepository;

    @InjectMocks
    private TransactionService serviceInTest;

    @BeforeEach
    void setup() {
        openMocks(this);
    }

    @Test
    public void whenEmptyIdList_ReturnsAllRecords() {
        // given
        when(transactionRepository.findAll()).thenReturn(buildDefaultTransactionEntities());
        when(customerRepository.findAllById(Lists.newArrayList(1, 2))).thenReturn(buildDefaultCustomerEntities());
        when(accountTypeRepository.findAllById(Lists.newArrayList(1, 2))).thenReturn(buildDefaultAccountTypeEntities());

        // when
        List<Transaction> transactions = serviceInTest.getTransactions(Lists.newArrayList(), Lists.newArrayList());

        // then
        assertEquals(buildDefaultTransactions(), transactions);
    }

    @Test
    public void givenAccountTypeIdList_ReturnsAllRecordsWithAccountType() {
        // given
        List<Integer> accountTypeIds = Lists.newArrayList(1);
        when(transactionRepository.findByAccountTypeIdIn(accountTypeIds))
            .thenReturn(buildDefaultTransactionEntities().stream()
                                                         .filter(t -> accountTypeIds.contains(t.getAccountTypeId()))
                                                         .collect(Collectors.toList()));

        when(accountTypeRepository.findAllById(accountTypeIds))
            .thenReturn(buildDefaultAccountTypeEntities().stream()
                                                         .filter(at -> accountTypeIds.contains(at.getId()))
                                                         .collect(Collectors.toList()));
        when(customerRepository.findAllById(Lists.newArrayList(1, 2))).thenReturn(buildDefaultCustomerEntities());

        // when
        List<Transaction> transactions = serviceInTest.getTransactions(accountTypeIds, Lists.newArrayList());

        // then
        assertEquals(buildDefaultTransactions().stream()
                                               .filter(t -> t.getAccountType().equals("saving_account"))
                                               .collect(Collectors.toList()),
                     transactions);
    }

    @Test
    public void givenCustomerIdList_ReturnsAllRecordsForCustomer() {
        // given
        List<Integer> customerIds = Lists.newArrayList(1);
        when(transactionRepository.findByCustomerIdIn(customerIds))
            .thenReturn(buildDefaultTransactionEntities().stream()
                                                         .filter(t -> customerIds.contains(t.getCustomerId()))
                                                         .collect(Collectors.toList()));

        when(accountTypeRepository.findAllById(Lists.newArrayList(1, 2))).thenReturn(buildDefaultAccountTypeEntities());
        when(customerRepository.findAllById(customerIds))
            .thenReturn(buildDefaultCustomerEntities().stream()
                                                      .filter(customer -> customerIds.contains(customer.getId()))
                                                      .collect(Collectors.toList()));

        // when
        List<Transaction> transactions = serviceInTest.getTransactions(Lists.newArrayList(), customerIds);

        // then
        assertEquals(buildDefaultTransactions().stream()
                                               .filter(t -> t.getCustomerLastName().equals("Kowalski"))
                                               .collect(Collectors.toList()),
                     transactions);
    }

    @Test
    public void givenBothIdLists_ReturnsFilteredRecords() {
        // given
        List<Integer> customerIds = Lists.newArrayList(1);
        List<Integer> accountTypeIds = Lists.newArrayList(1);
        when(transactionRepository.findByCustomerIdInAndAccountTypeIdIn(customerIds, accountTypeIds))
            .thenReturn(buildDefaultTransactionEntities().stream()
                                                         .filter(t -> customerIds.contains(t.getCustomerId()))
                                                         .filter(t -> accountTypeIds.contains(t.getAccountTypeId()))
                                                         .collect(Collectors.toList()));

        when(accountTypeRepository.findAllById(accountTypeIds))
            .thenReturn(buildDefaultAccountTypeEntities().stream()
                                                         .filter(at -> accountTypeIds.contains(at.getId()))
                                                         .collect(Collectors.toList()));
        when(customerRepository.findAllById(customerIds))
            .thenReturn(buildDefaultCustomerEntities().stream()
                                                      .filter(customer -> customerIds.contains(customer.getId()))
                                                      .collect(Collectors.toList()));

        // when
        List<Transaction> transactions = serviceInTest.getTransactions(accountTypeIds, customerIds);

        // then
        assertEquals(buildDefaultTransactions().stream()
                                               .filter(t -> t.getCustomerLastName().equals("Kowalski"))
                                               .filter(t -> t.getAccountType().equals("saving_account"))
                                               .collect(Collectors.toList()),
                     transactions);
    }

    private List<TransactionEntity> buildDefaultTransactionEntities() {
        List<TransactionEntity> result = new ArrayList<>();
        result.add(new TransactionEntity(
            1,
            new BigDecimal(1000),
            1,
            1,
            "2020-12-05"
        ));

        result.add(new TransactionEntity(
            2,
            new BigDecimal(2000),
            2,
            1,
            "2020-12-04"
        ));

        result.add(new TransactionEntity(
            3,
            new BigDecimal(3000),
            1,
            2,
            "2020-12-06"
        ));

        result.add(new TransactionEntity(
            4,
            new BigDecimal(4000),
            2,
            2,
            "2020-12-10"
        ));

        return result;
    }

    private List<Transaction> buildDefaultTransactions() {
        List<Transaction> result = new ArrayList<>();
        result.add(new Transaction(
            "2020-12-05",
            new BigDecimal(1000),
            1,
            "saving_account",
            "Jan",
            "Kowalski"
        ));

        result.add(new Transaction(
            "2020-12-04",
            new BigDecimal(2000),
            2,
            "checking_account",
            "Jan",
            "Kowalski"
        ));

        result.add(new Transaction(
            "2020-12-06",
            new BigDecimal(3000),
            3,
            "saving_account",
            "Andrzej",
            "Nowak"
        ));

        result.add(new Transaction(
            "2020-12-10",
            new BigDecimal(4000),
            4,
            "checking_account",
            "Andrzej",
            "Nowak"
        ));

        return result;
    }

    private List<CustomerEntity> buildDefaultCustomerEntities() {
        List<CustomerEntity> result = new ArrayList<>();
        result.add(new CustomerEntity(
            1,
            "Jan",
            "Kowalski",
            new BigDecimal(1000)
        ));

        result.add(new CustomerEntity(
            2,
            "Andrzej",
            "Nowak",
            new BigDecimal(2000)
        ));

        return result;
    }

    private List<AccountTypeEntity> buildDefaultAccountTypeEntities() {
        List<AccountTypeEntity> result = new ArrayList<>();
        result.add(new AccountTypeEntity(
            1,
            "saving_account"
        ));

        result.add(new AccountTypeEntity(
            2,
            "checking_account"
        ));

        return result;
    }
}
