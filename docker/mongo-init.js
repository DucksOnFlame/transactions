db.auth('mongoadmin', 'mongoadmin')
db = db.getSiblingDB('transactions')

db.createUser(
    {
        user: "mongoadmin",
        pwd: "mongoadmin",
        roles: [
            {
                role: "readWrite",
                db: "transactions"
            }
        ]
    }
);
